'use strict'
/**
*  Interface to CALE - AI
*
*
* @class CaleHolism
* @package    CaleAI-interface
* @copyright  Copyright (c) 2022 James Littlejohn
* @license    http://www.gnu.org/licenses/old-licenses/gpl-3.0.html
* @version    $Id$
*/
import util from 'util'
import events from 'events'

var CaleHolism = function () {
  events.EventEmitter.call(this)
  console.log(`caleAI--{{hello}}`)
}

/**
* inherits core emitter class within this class
* @method inherits
*/
util.inherits(CaleHolism, events.EventEmitter)

/**
* NLP conversation
* @method NLPflow
*
*/
CaleHolism.prototype.nlpflow = function (inFlow) {
  // pass to validtor FIRST TODO
  let outFlow = {}
  outFlow.type = 'caleai'
  outFlow.action = 'npl-reply'
  if (inFlow.text === 'hello') {
    outFlow.data = 'hello how can CALE help?'
  } else if (inFlow.text === 'chart last week daily') {
    outFlow.data = 'working on preparing those charts, please wait'
  } else if (inFlow.text === 'happy') {
    outFlow.data = 'hello how can CALE help?'
  } else if (inFlow.text === 'sad') {
    outFlow.data = 'hello how can CALE help?'
  } else if (inFlow.text === 'family') {
    outFlow.data = 'All those on the Internet are family'
  } else {
    outFlow.data = 'sorry CALE cannot help.  CALE is still learning.'
  }
  return outFlow
}

/**
* Predict time series
* @method predictFlow
*
*/
CaleHolism.prototype.predictflow = function () {
  let outFlow = {}
  outFlow.type = 'CALEAI'
  outFlow.action = 'prediction'
  if (inFLow.text === 'hello') {
    outFlow.data = 'This is not operational yet, still testing' // call prediction flow
  }
  return outFlow
}

export default CaleHolism
